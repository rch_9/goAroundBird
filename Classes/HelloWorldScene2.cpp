#include "HelloWorldScene2.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

Scene* HelloWorld2::createScene() {
    auto scene = Scene::create();
    auto layer = HelloWorld2::create();
    scene->addChild(layer);

    return scene;
}

bool HelloWorld2::init() {
    if (!Layer::init()) {
        return false;
    }

    return true;
}

void HelloWorld2::onEnter() {

}

bool HelloWorld2::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unused_event) {
    return true;
}

void HelloWorld2::update(float delta) {

}

void HelloWorld2::addPlayer() {

}

void HelloWorld2::addGem() {

}

void HelloWorld2::updateActions(float delta) {

}

bool HelloWorld2::isGameOver() const {
    return true;
}
