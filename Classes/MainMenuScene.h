#ifndef __MAINMENU_SCENE_H__
#define __MAINMENU_SCENE_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"

class MainMenu : public cocos2d::Layer {
public:
    static cocos2d::Scene* createScene();
    virtual bool init();

    void pageViewEvent(Ref *pSender, cocos2d::ui::PageView::EventType type);
    void initShopButton();
    void initPageView();

    CREATE_FUNC(MainMenu);
    void onButtonClicked(Ref *sender, cocos2d::ui::Widget::TouchEventType type);
    void onShopClicked(Ref *sender, cocos2d::ui::Widget::TouchEventType type);
};

#endif // __MAINMENU_SCENE_H__
