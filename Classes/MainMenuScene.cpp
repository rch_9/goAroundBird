#include "MainMenuScene.h"
#include "HelloWorldScene.h"
#include "HelloWorldScene2.h"
#include "ShopScene.h"
#include "Managers/DatabaseManager.h"

//#include "cocos2d/external/json/rapidjson.h"

#include "json/rapidjson.h"
#include "json/document.h"

USING_NS_CC;
using namespace cocos2d::ui;

Scene* MainMenu::createScene() {
    auto scene = Scene::create();
    auto layer = MainMenu::create();
    scene->addChild(layer);

    return scene;
}

bool MainMenu::init() {
    if (!Layer::init()) {
        return false;
    }


    std::string str = "{\"hello\" : \"word\"}";
    rapidjson::Document d;
    d.Parse<0>(str.c_str());
    if (d.HasParseError()) {
    CCLOG("GetParseError %s\n",d.GetParseError());
    } else if (d.IsObject() && d.HasMember("hello")) {
    CCLOG("%s\n", d["hello"].GetString());
    }

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    DataBaseManager *sql = DataBaseManager::getInstance();
    sql->insert("foo", "value1");
    std::string value = sql->select("foo");
    auto label = Label::createWithTTF(d["hello"].GetString(),"fonts/Marker Felt.ttf",32);
    label->setPosition(100, 100);
    this->addChild(label);
#endif

    initPageView();
    initShopButton();

    return true;
}

void MainMenu::onButtonClicked(Ref* sender, Widget::TouchEventType type) {
    if (type == Widget::TouchEventType::ENDED) {
//        PageView.getCurPageIndex()
        auto v = static_cast<PageView*>(this->getChildByTag(0));
//        CCLOG("%d", v->getCurrentPageIndex());
        if (v->getCurrentPageIndex() == 0) {
            Director::getInstance()->pushScene(TransitionSlideInR::create(0.1, HelloWorld::createScene()));
        } else {
            Director::getInstance()->pushScene(TransitionSlideInR::create(0.1, HelloWorld2::createScene()));
        }


    }
}

void MainMenu::onShopClicked(Ref *sender, Widget::TouchEventType type) {
    if (type == Widget::TouchEventType::ENDED) {
        Director::getInstance()->pushScene(TransitionSlideInR::create(0.1, Shop::createScene()));
    }
}

void MainMenu::pageViewEvent(Ref *pSender, PageView::EventType type) {

}

void MainMenu::initShopButton() {
    Button *btn = Button::create("shop.png", "shop_c.png");
    btn->addTouchEventListener( CC_CALLBACK_2(MainMenu::onShopClicked, this));
    btn->setPosition(Vec2(this->getContentSize().width / 2.f, this->getContentSize().height - 100));
    this->addChild(btn);
}

void MainMenu::initPageView() {
    auto visibleSize = Director::getInstance()->getVisibleSize();

    Size size(500, 360);
    PageView* pageView = PageView::create();
    pageView->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    pageView->setDirection(PageView::Direction::HORIZONTAL);
    pageView->setContentSize(size);
    pageView->setPosition(visibleSize / 2.f);
    pageView->removeAllItems();
    pageView->setIndicatorEnabled(true);

    int pageCount = 2;
    for (int i = 0; i < pageCount; ++i) {
        Layout* layout = Layout::create();
        layout->setContentSize(size);

        ImageView* imageView = ImageView::create("view.png");
        imageView->setScale9Enabled(true);
        imageView->setContentSize(size);
        imageView->setPosition(Vec2(layout->getContentSize().width / 2.0f, layout->getContentSize().height / 2.0f));
        layout->addChild(imageView);

        Button *btn = Button::create("round.png", "round_c.png");
        btn->addTouchEventListener( CC_CALLBACK_2(MainMenu::onButtonClicked, this));
        btn->setPosition(Vec2(layout->getContentSize().width / 2.f, layout->getContentSize().height / 2.f));
        layout->addChild(btn);

        pageView->insertCustomItem(layout, i);
    }

    pageView->addEventListener(CC_CALLBACK_2(MainMenu::pageViewEvent, this));
    this->addChild(pageView, 1, 0);
}
