#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"
#include "MainMenuScene.h"
//#include "cocos2d/external/Box2D/Box2D.h"


USING_NS_CC;

Scene* HelloWorld::createScene() {
    auto scene = Scene::create();
    auto layer = HelloWorld::create();
    scene->addChild(layer);

    return scene;
}

bool HelloWorld::init() {
    if ( !Layer::init() ) {
        return false;
    }
    
    _minimalDeegre = 46;
    _currentRand = 30;
    _oldRand = 0;
    _flag = false;

    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("data.plist", "data.png");
    this->addPlayer();
    this->addClouds();
    this->addGem();

    this->schedule(schedule_selector(HelloWorld::updateActions), 1);
//    this->schedule(schedule_selector(HelloWorld::stepByBot), 0.066);
    this->scheduleUpdate();

    return true;
}


void HelloWorld::update(float delta) {

    if (isGameOver()) {
        Director::getInstance()->popScene();
        CCLOG("game over%f", delta);
    }
}


void HelloWorld::updateActions(float delta) {

    auto stick1 = this->getChildByTag(1);
    auto stick2 = this->getChildByTag(2);

    auto d1 = 3;
    auto d2 = 3;
    if (_flag) {
        d1 *= (-1);
    } else {
        d2 *= (-1);
    }
    _flag = !_flag;

    stick1->runAction(RotateBy::create(delta, _currentRand + d1));
    stick2->runAction(RotateBy::create(delta, _currentRand + d2));

    _currentRand = RandomHelper::random_int(20, 30);
}

void HelloWorld::stepByBot(float delta) {
    auto stick = static_cast<Sprite*>(this->getChildByTag(0));

    if (stick->getRotationSkewX() - getRot1() < 1.5) {
        stick->runAction(EaseSineOut::create(RotateBy::create(0.2, 20.f)));
    }
}

bool HelloWorld::isGameOver() const {
    auto stick = this->getChildByTag(0);

    auto rot = stick->getRotationSkewX();

    if ((getRot2() - rot < 0) || (rot - getRot1() < 0)) {
        CCLOG("%f", getRot2() - getRot1());
        return true;
    }

    return false;
}

float HelloWorld::getRot1() const {
    return this->getChildByTag(1)->getRotationSkewX() + 10;
}

float HelloWorld::getRot2() const {
    return this->getChildByTag(2)->getRotationSkewX() - 11;
}

void HelloWorld::onEnter() {
    Layer::onEnter();

    auto listener = EventListenerTouchOneByOne::create();
    listener->onTouchBegan = CC_CALLBACK_2(HelloWorld::onTouchBegan, this);
    listener->onTouchEnded = CC_CALLBACK_2(HelloWorld::onTouchEnded, this);

    auto dispatcher = cocos2d::Director::getInstance()->getEventDispatcher();
    dispatcher->addEventListenerWithSceneGraphPriority(listener, this);
}

bool HelloWorld::onTouchBegan(Touch *touch, Event *unused_event) {
    auto stick = static_cast<Sprite*>(this->getChildByTag(0));
    stick->runAction(EaseSineOut::create(RotateBy::create(0.2, 20.f)));

    Vector<SpriteFrame*> vect;
    vect.reserve(2);
    vect.pushBack(SpriteFrameCache::getInstance()->getSpriteFrameByName("frame-2.png"));
    vect.pushBack(SpriteFrameCache::getInstance()->getSpriteFrameByName("frame-1.png"));
    auto a = Animate::create(Animation::createWithSpriteFrames(vect, 0.1));
    static_cast<Sprite*>(stick->getChildByTag(0))->runAction(a);


    return true;
}

void HelloWorld::addPlayer() {
    auto visibleSize = Director::getInstance()->getVisibleSize();
    auto stick = Sprite::createWithSpriteFrameName("stick.png");
    stick->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    stick->setPosition(visibleSize / 2.f);
//    stick->setRotation(-90  + _minimalDeegre / 2 - 11);
//    stick->setRotation(-90  - _minimalDeegre / 2 + 10);
    stick->setRotation(-90);
    this->addChild(stick, 1, 0);
    auto player = Sprite::createWithSpriteFrameName("frame-1.png");
    player->setRotation(90);
    player->setPosition(stick->getContentSize().width, stick->getContentSize().height / 2.f);
    player->setTag(0);
    stick->addChild(player);
}

void HelloWorld::addGem() {
    auto visibleSize = Director::getInstance()->getVisibleSize();
    auto stick = Sprite::createWithSpriteFrameName("stick.png");
    stick->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    stick->setPosition(visibleSize / 2.f);
    stick->setRotation(90);
    this->addChild(stick, 1, 3);

    auto gem = Sprite::create("gem.png");
    gem->setPosition(stick->getContentSize().width, stick->getContentSize().height / 2.f);
    gem->setRotation(90.f);
    stick->addChild(gem);
}

void HelloWorld::addClouds() {
    auto visibleSize = Director::getInstance()->getVisibleSize();

    auto circle = Sprite::createWithSpriteFrameName("round.png");
    circle->setPosition(visibleSize / 2.f);
    this->addChild(circle, 0);

    auto stick1 = Sprite::createWithSpriteFrameName("stick.png");
    stick1->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    stick1->setPosition(visibleSize / 2.f);
    stick1->setRotation(-90 - _minimalDeegre / 2);
    this->addChild(stick1, 1, 1);
    auto cloud1 = Sprite::createWithSpriteFrameName("BadCloud.png");
    cloud1->setPosition(stick1->getContentSize().width, stick1->getContentSize().height / 2.f);
    cloud1->setRotation(90);
    stick1->addChild(cloud1);

    auto stick2 = Sprite::createWithSpriteFrameName("stick.png");
    stick2->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    stick2->setPosition(visibleSize / 2.f);
    stick2->setRotation(-90 + _minimalDeegre / 2);
    this->addChild(stick2, 1, 2);
    auto cloud2 = Sprite::createWithSpriteFrameName("BadCloud.png");
    cloud2->setPosition(stick2->getContentSize().width, stick1->getContentSize().height / 2.f);
    cloud2->setRotation(90);
    stick2->addChild(cloud2);
}



void HelloWorld::onTouchEnded(Touch *touch, Event *unused_event) {
//    auto player = static_cast<Sprite*>(this->getChildByTag(0)->getChildByTag(0));
//    player->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("frame-1.png"));
}
