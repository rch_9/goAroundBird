#ifndef __SHOP_SCENE_H__
#define __SHOP_SCENE_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"

class Shop : public cocos2d::Layer {
public:
    static cocos2d::Scene* createScene();
    virtual bool init();

//    void pageViewEvent(Ref *pSender, cocos2d::ui::PageView::EventType type);
//    void initShop();
//    void initPageView();

    CREATE_FUNC(Shop);
//    void onButtonClicked(Ref *sender, cocos2d::ui::Widget::TouchEventType type);
    void onShopClicked(Ref *sender, cocos2d::ui::Widget::TouchEventType type);
    void onBackClicked(Ref *sender, cocos2d::ui::Widget::TouchEventType type);
    void onButtonClicked(Ref *sender, cocos2d::ui::Widget::TouchEventType type);
private:
    void addImageView(int ind1, int ind2);
    void initYesNo(Ref* sender);
};

#endif // __SHOP_SCENE_H__
