#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"

class HelloWorld : public cocos2d::Layer {
public:
    static cocos2d::Scene* createScene();

    virtual bool init();
    virtual void onEnter();
    virtual bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unused_event);
    virtual void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *unused_event);
    virtual void update(float delta);    
        
    CREATE_FUNC(HelloWorld);
    void addClouds();
    void addPlayer();
    void addGem();

    void updateActions(float delta);
    void stepByBot(float delta);

private:
    inline bool isGameOver() const;

    inline float getRot1() const;
    inline float getRot2() const;

    int _minimalDeegre;
    int _oldRand;
    int _currentRand;
    bool _flag;
};

#endif // __HELLOWORLD_SCENE_H__
