#ifndef DATABASEMANAGER_H
#define DATABASEMANAGER_H

#include "cocos2d.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

#include "sqlite/sqlite3.h"
//#include "sqlite/sqlite3ext.h"
//#include "sqlite3.h"
//#include "sqlite3ext.h"

class DataBaseManager : public cocos2d::Ref {
public:
    bool init();
    CREATE_FUNC(DataBaseManager);

private:
    DataBaseManager();
    static DataBaseManager* instance;
    sqlite3 *_db;
    bool open();
    void close();
public:
    virtual ~DataBaseManager();
    static DataBaseManager* getInstance();
    void insert(std::string key, std::string value);
    std::string select(std::string key);
//    ~DataBaseManager();
};

#endif // CC_PLATFORM

#endif // DATABASEMANAGER_H
