#include "ShopScene.h"

USING_NS_CC;

Scene *Shop::createScene() {
    auto scene = Scene::create();
    auto layer = Shop::create();
    scene->addChild(layer);

    return scene;
}

bool Shop::init() {
    if (!Layer::init()) {
        return false;
    }

    ui::Button *btn = ui::Button::create("clothes.png", "clothes_c.png");
    btn->addTouchEventListener( CC_CALLBACK_2(Shop::onShopClicked, this));
    btn->setPosition(Vec2(this->getContentSize().width / 2.f, this->getContentSize().height - 300));
    this->addChild(btn);

    ui::Button *btn2 = ui::Button::create("world.png", "world_c.png");
    btn2->addTouchEventListener( CC_CALLBACK_2(Shop::onBackClicked, this));
    btn2->setPosition(Vec2(this->getContentSize().width / 2.f, 300));
    this->addChild(btn2);

    ui::Button *btnback = ui::Button::create("back.png", "back_c.png");
    btnback->addTouchEventListener( CC_CALLBACK_2(Shop::onBackClicked, this));
    btnback->setPosition(Vec2(100, 100));
    this->addChild(btnback);

    return true;
}

void Shop::onShopClicked(Ref *sender, ui::Widget::TouchEventType type) {
    if (type == ui::Widget::TouchEventType::ENDED) {
        static_cast<ui::Button*>(sender)->setVisible(false);

        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 3; ++j) {
                addImageView(j, i);
            }
        }
    }
}

void Shop::onBackClicked(Ref *sender, ui::Widget::TouchEventType type) {
    if (type == ui::Widget::TouchEventType::ENDED) {
        Director::getInstance()->popScene();
    }
}

void Shop::onButtonClicked(Ref *sender, ui::Widget::TouchEventType type) {
    if (type == ui::Widget::TouchEventType::ENDED) {
        auto item = static_cast<ui::Widget*>(sender);
        if (!item->getName().compare("yes") || !item->getName().compare("no")) {
            item->getParent()->removeFromParent();
        } else {
            initYesNo(sender);
        }
    }
}

void Shop::addImageView(int ind1, int ind2) {
    // Create the imageview

    auto visibleSize = Director::getInstance()->getVisibleSize();

    Vec2 position = Vec2((visibleSize.width / 3.f - 50) * (ind1 + 1), (visibleSize.height / 3.f - 50) * (ind2 + 1));

    char buffer[15];
    sprintf (buffer, "skins/skin%d.png", ind1 + ind2*3);
    ui::ImageView* imageView = ui::ImageView::create(buffer);
    imageView->setName(buffer);
    imageView->ignoreContentAdaptWithSize(false);
    imageView->setScale9Enabled(true);
    imageView->setCapInsets(Rect(20,20,20,20));
    imageView->setPosition(position);

    imageView->setTouchEnabled(true);
    imageView->addTouchEventListener(CC_CALLBACK_2(Shop::onButtonClicked, this));

    this->addChild(imageView);
}

void Shop::initYesNo(Ref *sender) {
    auto visibleSize = Director::getInstance()->getVisibleSize();

    auto imageView = ui::ImageView::create("view.png");
    imageView->setPosition(visibleSize / 2.f);
    this->addChild(imageView);

    auto btnYes = ui::Button::create("yes.png", "yes_c.png");
    btnYes->setPosition(Vec2(50, imageView->getContentSize().height / 2.f));
    btnYes->setName("yes");
    btnYes->addTouchEventListener(CC_CALLBACK_2(Shop::onButtonClicked, this));
    imageView->addChild(btnYes);

    auto btnNo = ui::Button::create("no.png", "no_c.png");
    btnNo->setPosition(Vec2(imageView->getContentSize().width - 50, imageView->getContentSize().height / 2.f));
    btnNo->setName("no");
    btnNo->addTouchEventListener(CC_CALLBACK_2(Shop::onButtonClicked, this));
    imageView->addChild(btnNo);

    auto item = static_cast<ui::ImageView*>(sender)->clone();
    item->setPosition(Vec2(imageView->getContentSize().width / 2.f, imageView->getContentSize().height / 2.f));
    imageView->addChild(item);
}
