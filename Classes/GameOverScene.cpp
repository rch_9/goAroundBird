#include "GameOverScene.h"

USING_NS_CC;

Scene *GameOver::createScene() {
    auto scene = Scene::create();
    auto layer = GameOver::create();
    scene->addChild(layer);

    return scene;
}

bool GameOver::init() {
    if(!Layer::init()) {
        return false;
    }

    return true;
}
